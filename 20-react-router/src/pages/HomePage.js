import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

const HomePage = props => {
    const navigate = useNavigate();

    const navigateHandler = () => {
navigate('/products');
    }

    return (
        <> 
        <h1>My home page</h1>
<p><Link to="/products">List of product</Link></p>
        <p>
            <button onClick={navigateHandler}>Click this</button>
        </p>
        </>
       
    );
};

export default HomePage;