import React from 'react';
import { Link } from 'react-router-dom';

const PRODUCTS = [
    {id:'p1', title:'PRoduct 1'},
    {id:'p2', title:'PRoduct 2'},
    {id:'p3', title:'PRoduct 3'},
];
const ProductsPage = () => {
    return (
        <>
        <h1>
            the product page
        </h1>
        <ul>
            {PRODUCTS.map((prod) => (
                <li key={prod.id}>
                    <Link to={`/products/${prod.id}`}>{prod.title}</Link>
                </li>
            ))}
        </ul>
        </>
    );
};

export default ProductsPage;