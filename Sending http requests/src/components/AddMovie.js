import React, { useRef } from 'react';
import classes from './AddMovie.module.css';

const AddMovie = props => {
    const titleRef = useRef('');
    const openingTextRef = useRef('');
    const releaseDateRef = useRef('');

    const submitHandler = (event) => {
        event.preventDefault();

        const movie = {
            title: titleRef.current.value,
            openingText: openingTextRef.current.value,
            releaseDate: releaseDateRef.current.value,
        };

        props.onAddMovie(movie);
    }

    return (
        <form onSubmit={submitHandler}>
            <div className={classes.control}>
                <label htmlFor='title'>Title</label>
                <input type='text' id='title' ref={titleRef}/>
            </div>
            <div className={classes.control}>
                <label htmlFor='opening-text'>Opening text</label>
                <textarea rows='5' id='opening-text' ref={openingTextRef}></textarea>
            </div>
            <div className={classes.control}>
                <label htmlFor='date'>Release date</label>
                <input type='text' id='date' ref={releaseDateRef}/>
            </div>
            <button>Add movie</button>
        </form>
    );
};

export default AddMovie;