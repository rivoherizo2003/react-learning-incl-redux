import addition from "./Comparator";

test("test addition", () => {
    expect(addition(2,2)).toBe(4);
    expect(addition(2,2)).toEqual(4);
});