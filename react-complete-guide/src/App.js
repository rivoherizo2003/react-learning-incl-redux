import logo from "./logo.svg";
import "./App.css";
import ExpensesList from "./components/Expenses/ExpensesList";
import NewExpense from "./components/NewExpense/NewExpense";
import { useState } from "react";
import Expenses from "./components/Expenses/Expenses";

function App() {
  const DUMMY_EXPENSES = [
    {id:1, title: "Car ", amount: 78.3, date: new Date(2021, 5, 7) },
    {id:12, title: "Car inssurance", amount: 28.3, date: new Date(2021, 2, 12) },
    {id:13, title: "New Tv", amount: 98.3, date: new Date(2022, 6, 5) },
    {id:14, title: "Dentifrice", amount: 50.3, date: new Date(2020, 10, 6) },
    {id:15, title: "Laptop", amount: 10.3, date: new Date(2020, 11, 9) },
    {id:158, title: "Pencil", amount: 70.3, date: new Date(2020, 11, 9) },
  ];
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const getDataFromChild = (enteredData) => {
    setExpenses(prevState => {return [enteredData, ...DUMMY_EXPENSES];});
  }

  return (
    <div>
      <NewExpense onEventFromApp={getDataFromChild}/>
      <Expenses expenses={expenses}></Expenses>
    </div>
  );
}

export default App;
