import { useState } from "react";
import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
  const [isFormNewExpenseShown, setIsFromNewExpenseShown] = useState(false);
  const saveNewExpense = (enteredData) => {
    const newExpenseData = {...enteredData, id: Math.random().toString};
    props.onEventFromApp(newExpenseData);
  };
  const showFormNewExpense = () => {
    setIsFromNewExpenseShown(true);
  }

  const hideFormNewExpense = () => {
    setIsFromNewExpenseShown(false);
  }

  let newExpenseContent = <button type="button" onClick={showFormNewExpense}>Add new expense</button>
  if (isFormNewExpenseShown){
    newExpenseContent = <ExpenseForm onSaveNewExpense={saveNewExpense} onCancelTriggered={hideFormNewExpense} />;
  }

  return (
    <div className="new-expense">
      {newExpenseContent}
    </div>
  );
};

export default NewExpense;
