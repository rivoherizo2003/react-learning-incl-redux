import { useState } from "react";
import ExpenseItem from "./ExpenseItem";

import Card from "../UI/Card";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesList from "./ExpensesList";
import "./Expenses.css";
import ExpensesChart from "./ExpensesChart";

function Expenses(props) {
  const [pickedYear, setPickedYear] = useState("2022");
  const selectYear = (filterData) => {
    setPickedYear(filterData.year);
  };

  let expensesFiltered = props.expenses.filter((el) => el.date.getFullYear().toString() === pickedYear);

  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter onSelectYear={selectYear}></ExpensesFilter>
        <ExpensesChart expenses={expensesFiltered}/>
        <ExpensesList expenses={expensesFiltered}/>
      </Card>
    </div>
  );
}

export default Expenses;