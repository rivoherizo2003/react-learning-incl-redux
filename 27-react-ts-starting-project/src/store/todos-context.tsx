import React, { useState } from "react";
import Todo from "../models/todo";
type TodosContextObj = {
    items: Todo[];
addTodo: (text: string) => void;
removeTodo:(id:string) => void;
};

export const TodoContext = React.createContext<TodosContextObj>({
    items: [],
    addTodo:() => {},
    removeTodo: (id: string) => {}
});

const TodosContextProvider: React.FC = (props) => {
    const [todos, setTodos] = useState<Todo[]>([]);

    const addTodoHandler = (todoText: string) => {
      const newTodo = new Todo(todoText);
      setTodos((prev) => {
        return prev.concat(newTodo);
      });
    };
  
    const onRemoveTodoHandler = (id: string) => {
      setTodos((prev) => {
        return prev.filter(todo => todo.id !== id);
      })
    };

    const contextValue:TodosContextObj = {
        items: todos,
        addTodo: addTodoHandler,
        removeTodo: onRemoveTodoHandler
    }

    return <TodoContext.Provider value={contextValue}>{props.children}</TodoContext.Provider>;
}

export default TodosContextProvider;