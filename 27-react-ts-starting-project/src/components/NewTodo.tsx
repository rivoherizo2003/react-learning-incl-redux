import React, { useContext, useRef } from "react";
import classes from "./NewTodo.module.css";
import { TodoContext } from "../store/todos-context";

const NewTodo: React.FC = () => {
  const todoCtx = useContext(TodoContext);
  const useRefText = useRef<HTMLInputElement>(null);
  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();

    const enteredText = useRefText.current!.value;
    if (enteredText.trim().length === 0) return;

    todoCtx.addTodo(enteredText);
  };

  return (
    <form onSubmit={submitHandler} className={classes.form}>
      <label htmlFor="text">Todo text</label>
      <input type="text" ref={useRefText} id="text" />
      <button>Add todo</button>
    </form>
  );
};

export default NewTodo;
